This dashboard provides supplementary information to the article *How do modeling choices impact the representation of structural connectivity and the dynamics of suspended sediment fluxes in distributed soil erosion models?* by Magdalena Uber, Guillaume Nord, Cédric Legout and Luis Cea.

The figure shows the contribution of erosion zones that act as sediment sources to total suspended sediment load in percent simulated with an adapted version of the soil erosion model described in Cea et al., 2015 and implemented in Iber (Bladé et al., 2014; [www.iberaula.es](https://www.iberaula.es)). Model output can be visualized for two mesoscale Mediterranean catchments in southeastern France, the 42 km<sup>2</sup> Claduègne catchment (Nord et al., 2017) and the 20 km<sup>2</sup> Galabre catchment (Esteves et al., 2019) and for different sets of scenarios:

- CDA threshold: The threshold of contributing drainage area (CDA) defines the length of the river network (Tarboton et al., 1991). Values were varied from 15 ha to 500 ha.

- Manning's *n*: river: Manning's roughness parameter in the river network. Values were varied from 0.025 to 0.1.

- Manning's *n*: hillslopes: Manning's roughness parameter on the hillslopes. Values were varied from 0.2 to 0.8.

- Source classification: Source classification based on connectivity, i.e. sediment sources were subdivided based on their distance to the outlet and their distance to the river.

In addition to the modeled source contributions the time series of rainfall intensity, liquid and solid discharge can be displayed.

### References:

Bladé, E., Cea, L., Corestein, G., Escolano, E., Puertas, J., Vázquez-Cendón, E., Dolz, J. and Coll, A. (2014). Iber: herramienta de simulación numérica del flujo en ríos. *Revista Internacional de Métodos Numéricos para Cálculo y Dise~no en Ingeniería*, 30(1): 1 - 10.

Cea, L., Legout, C., Grangeon, T., and Nord, G. (2015). Impact of model simplifications on soil erosion predictions: application of the GLUE methodology to a distributed event-based model at the hillslope scale. *Hydrological Processes*, 30(7): 1096 - 1113.

Esteves, M., Legout, C., Navratil, O., and Evrard, O. (2019). Medium term high frequency observation of discharges and suspended sediment in a Mediterranean mountainous catchment. *Journal of Hydrology*, 568: 562 - 574.

Nord, G., Boudevillain, B., Berne, A., Branger, F., Braud, I., Dramais, G., Gérard, S., Le Coz, J., Legout, C., Molinié, G., Van Baelen, J., Vandervaere, J.-P., Andrieu, J., Aubert, C., Calianno, M., Delrieu, G., Grazioli, J., Hachani, S., Horner, I., Huza, J., Le Boursicaud, R., Raupach, T. H., Teuling, A. J., Uber, M., Vincendon, B., and Wijbrans, A. (2017). A high space-time resolution dataset linking meteorological forcing and hydro-sedimentary response in a mesoscale Mediterranean catchment (Auzon) of
the Ardèche region, France. *Earth System Science Data*, 9(1): 221 - 249.

Tarboton, D. G., Bras, R. L., and Rodriguez-Iturbe, I. (1991). On the extraction of channel networks from digital elevation data. *Hydrological Processes*, 5(1): 81 - 100.

Uber, M., Nord, G., Legout, C. and Cea, L. How do modeling choices impact the representation of structural connectivity and the dynamics of suspended sediment fluxes in distributed soil erosion models? *Submitted to Earth Surface Dynamics*

