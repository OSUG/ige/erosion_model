#!/bin/bash

NAME=shiny_erosion_model
VERSION=0.2

docker build -t $NAME:$VERSION --rm .

CURRENT=`pwd`

docker run --rm -p 80:3838 $NAME:$VERSION

