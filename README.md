# Erosion Model

Public Code with Data for the article *How do modeling choices impact the representation of structural connectivity and the dynamics of suspended sediment fluxes in distributed soil erosion models?* by Magdalena Uber, Guillaume Nord, Cédric Legout and Luis Cea.

# Application:

Requirements: R v3.6.1 + libraries "shiny", "shinydashboard" and "markdown"

License: GPLv2 (code) + CC-BY-SA for Data

It uses the base docker image *rocker/shiny* as runtime environment: see https://hub.docker.com/r/rocker/shiny

- Install missing library:
    - grep library shinyapps/erosion_model/*
        grep: shinyapps/erosion_model/Data: est un dossier
        shinyapps/erosion_model/functions.R:library(shiny)
        shinyapps/erosion_model/functions.R:library(shinydashboard)
        shinyapps/erosion_model/functions.R:library(markdown)

    - fix Dockerfile to install R dependencies

- Build & run image:
    ./run_docker.sh

