FROM gricad-registry.univ-grenoble-alpes.fr/osug/dc/shiny-base:4.0.2

# Install dependencies and clean tmp files
RUN install.r \
      shinydashboard \
      markdown \
      && rm -rf /tmp/downloaded_packages

# Copy app files to /srv/shiny
COPY shinyapps/erosion_model /srv/shiny/

